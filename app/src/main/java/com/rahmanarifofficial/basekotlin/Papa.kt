package com.rahmanarifofficial.basekotlin

/*
* Untuk melakukan inheritance, Anda harus membuat sebuah parent class yang diwarisi oleh child class
* Analoginya seperti ini Papa -> Anak
* Anak mewarisi Papa
* parent class di tandai dengan keyword open class
* */
open class Papa {

    var name = "Andrea"
    private var umur = 50

    /*
    * Perbedaan antara inheritance dan polymorphism
    * Ketika function tersebut tidak di redefined di child class
    * seperti dibawah ini,
    * child class bisa memanggil function sifat() //TODO : Cek dibaris 100 dan 105
    * child class juga bisa memanggil pengetahuan secara tidak langsung namun
    * child class juga bisa meredefined function pengetahuan //TODO: Cek di baris 42
    * inilah yang disebut polymorphism, function tersebut punya banyak bentuk
    * */
    fun sifat() {
        println("Saya orang yang baik")
    }

    open fun pengetahuan() {
        println("Saya tau menjalani hidup")
    }

}

/*
* Untuk child class harus ditandai dengan : (titik dua)
* seperti kode dibawah
* */
class Anak : Papa() {
    private var childname = "Jhony"
    private var umur = 25

    fun silsilah() = "Saya ${this.childname}, Anak dari bapak ${name}"

    override fun pengetahuan() {
        println("Saya tau tentang hidup menggunakan teknologi")
    }
}

class Anak2 : Papa() {

    override fun pengetahuan() {
        println("Saya tau tentang hidup menggunakan mobil saja")
    }
}

open class Employee(private var name: String, private var salary: Int) {

    /*
    * Anda dapat memodifikasi sifat yang dimiliki oleh child class,
    * Namun di parent class harus ada keyword open
    * */
    open fun getSalary() = salary
}

class Manager(
    private var managerName: String,
    private var managerSalary: Int,
    private var managerTunjangan: Int
) : Employee(managerName, managerSalary) {

    /*
    * Anda dapat memodifikasi disini
    * */
    override fun getSalary(): Int {
        return managerSalary + managerTunjangan
    }
}

class Programmer(
    private var programmerName: String,
    private var programmerSalary: Int,
    private var programmerBonus: Int
) : Employee(programmerName, programmerSalary) {
    override fun getSalary(): Int {
        return programmerSalary + (programmerBonus / 4)
    }
}

fun main() {
    /*
    * Inti dari pewarisan (inheritance) adalah
    * Child class dapat memanggil function yang ada di parent class
    * Namun Parent class tidak bisa memanggil function yang ada di child class
    * seperti halnya pewarisan dengan analogi tadi
    * seluruh sifat yang dimiliki oleh Papa bisa saja dimiliki oleh Anak
    * Namun Papa tidak bisa memiliki sifat yang dimiliki oleh Anak
    * */

    val papa = Papa()
    papa.sifat()
    papa.silsilah()

    val anak = Anak()
    anak.sifat()
    anak.silsilah()

    val anak2 = Anak2()
    anak2.sifat()

    val staff = Employee("Steve", 10000000)
    println(staff.getSalary())

    val boss = Manager("Andrea", 20000000, 1000000)
    println(boss.getSalary())
}
