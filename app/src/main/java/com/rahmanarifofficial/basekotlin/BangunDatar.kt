package com.rahmanarifofficial.basekotlin

/*
* abstract class tidak harus ada abstract method
* namun, abstract method harus ada di abstract class
* */
abstract class BangunDatar() {
    abstract fun luas()
}

/*
* 1 child class hanya bisa mengimplement 1 parent class
* Namun bisa implement banyak interface
* Caranya tinggal pisahkan dengan koma (,)
* */

class Lingkaran : BangunDatar(), FungsiBangunDatar, FungsiBangunRuang {
    override fun volume() {

    }

    override fun keliling(example: Int) {
        println(example)
    }

    override fun luas() {

    }
}

/*
* Anda bisa langsung mengimplement sebuah interface dalam child class
* seperti dibawah ini
* */
class Tabung : FungsiBangunRuang {
    override fun volume() {

    }

}