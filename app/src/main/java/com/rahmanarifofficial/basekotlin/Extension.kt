package com.rahmanarifofficial.basekotlin

/*
* Di Kotlin,
* Anda bisa membuat sebuah extension yang mana function itu belum di ada dan di definisikan sebelumnya
* Cara mendefinisikannya seperti di bawah ini.
* */
fun Int.toHuruf() {
    /*
    * Keyword this itu merujuk kepada kelasnya,
    * Dalam konteks ini, this merujuk kepada Int
    * */
    if (this.equals(0)) {
        println("Nol")
    } else if (this.equals(1)) {
        println("Satu")
    } else if (this.equals(2)) {
        println("Dua")
    } else if (this.equals(3)) {
        println("Tiga")
    } else {
        println("Lebih dari 3")
    }
}

fun Int.diKali(x: Int): String {
    val hasilKali = this*x
    return "Hasil perkalian dari $this x $x adalah $hasilKali"
}

fun Int.gabungan(x: String) {
    println("$x gabungan $this")
}