package com.rahmanarifofficial.basekotlin

/*
* Disebuah Interface, Anda bisa membuat abstract function
* Berbeda dengan abstract function di abstract class, di Interface anda bisa membuat abstract function yang tidak mandatory
* Dengan membuat sebuah inisialisasi empty body seperti dibawah
* Terkadang ini dibutuhkan.
* */
interface FungsiBangunDatar {
    fun keliling(example: Int){}
}

/*
* Jika tidak ada inisialisasi empty body seperti dibawah
* Berarti function tersebut wajib diimplementasi
* */
interface FungsiBangunRuang {
    fun volume()
}