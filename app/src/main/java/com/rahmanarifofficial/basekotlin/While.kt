package com.rahmanarifofficial.basekotlin

fun main() {
    /*
    * Di Kotlin anda dapat menginisialisasi arraylist / mutablelist seperti dibawah
    * Any adalah bentuk tipe data, selayaknya String, Int, Double, etc
    * Any digunakan jika tipe datanya memang abstrak.
    * */
    val array = arrayListOf<Any>()

    array.add("Andrea")
    array.add(25)
    array.add(30f)

    // Dengan task yang sama kita gunakan while
    var iterator = 0
    while (iterator <= array.size - 1){
        println(array[iterator])
        iterator++
    }

    iterator = 0
    while (iterator <= array.size - 1){
        if (array[iterator].equals(30f)){
            array[iterator] = 30
        }
        iterator++
    }
    println(array)
}