package com.rahmanarifofficial.basekotlin

class Vehicle {
    /*
    * Istilah baru
    * - lateinit var itu memungkinkan anda untuk deklarasi variable tanpa harus diinisialisasi, Anda harus berhati hati menggunakan ini,
    * pastikan sebelum dipanggil, variable harus diinisiasi.
    * - null-safe pointer (?) memungkinkan anda untuk inisiasi variable dengan nilai null dan ini sangan membantu sebab anda tidak akan terkena
    * null pointer exception.
    * */

    var name: String? = null
    lateinit var model: String
    var typeFuel: String? = null
    var color: String? = null
    lateinit var colorType: Color

    /*
    * Anda bisa mendefinisikan lebih dari 1 constructor dengan inisiasi variable yang berbeda.
    * Ini disebut overloading constructor.
    * Jika hanya satu primary constructor, tapi Anda ingin mendefinisikan variable wajib anda bisa cek class Vehicle2
    * */
    constructor(name: String? = null) {
        this.name = name
    }

    constructor(name: String? = null, model: String? = null) {
        this.name = name
        /*
        * Anda akan menemukan error ketika menginisiasi sebuah variable non null dengan variable null
        * Anda harus menjadikan null-safe variable tadi menjadi variable non null
        * */
        this.model = model
        //Anda harus merubahnya menjadi
        if (model != null) {
            this.model = model
        }
        /*
        * atau anda dapat memaksakan variable tersebut menjadi variable non null, dan ini berbahaya,
        * jika variable itu null maka anda akan terkena null pointer exception
        * */
        this.model = model!!

    }

    /*
    * Deklarasi function di kotlin
    * Berikut merupukan void-type function
    * */
    fun display() {
        val varTypeFuel = if (typeFuel.isNullOrBlank()) "yang belum diketahui" else typeFuel
        println("Kendaraan ini bernama $name dengan model $model dan berbahan bakar $varTypeFuel")
    }

    /*
    * Anda juga bisa mendefinisikan non-void function
    * atau yang biasa dikenal return function
    * return function ini harus memiliki tipe data
    * biasanya Anda akan mendefinisikan seperti berikut
    * */

    fun print(): String {
        val varTypeFuel = if (typeFuel.isNullOrBlank()) "yang belum diketahui" else typeFuel
        return "Kendaraan ini bernama $name dengan model $model dan berbahan bakar $varTypeFuel"
    }

    /*
    * Uniknya di kotlin Anda bisa mendefinisikan secara langsung
    * tanpa harus berbentuk method pada umumnya
    * seperti dibawah ini,
    * Namun mendeklarasikan dengan cara ini hanya untuk method yang tanpa ada pengolahan data.
    * Lalu apa bedanya dengan mendeklarasikan sebuah variabel?
    * */
    fun printNamaKendaraan() = "Kendaraan ini bernama $name"
    var printNamaKendaraan = "Kendaraan ini bernama $name"

    /*
    * Berbeda dengan variabel, dengan function anda bisa membuat sebuah parameter variabel
    * Dalam kasus ini, printNamaKendaraan() ini merupakan overloading method
    *
    * Overloading method merupakan method dengan nama sama namun harus berbeda type parameternya.
    * */
    fun printNamaKendaraan(merek: String) = "Kendaraan ini bermerek $merek"
}

class Vehicle2(
    /*
    * Dalam membuat sebuah kelas, Anda dapat langsung set up constructor seperti di bawah
    * Anda dapat langsung set variable yang anda inginkan langsung
    * Ini disebut primary constructor.
    * */
    var name: String? = null, var model: String? = null, var typeFuel: String? = null
) {

    fun display() {
        val varTypeFuel = if (typeFuel.isNullOrBlank()) "Yang belum diketahui" else typeFuel
        println("Kendaraan ini bernama $name dengan model $model dan berbahan bakar $varTypeFuel")
    }
}

/*
* Anda bisa membuat method diluar dari sebuah class, namun ini tidak di rekomendasikan.
* Karena menjadikan kode menjadi tidak bersih.
* */
fun perkalian(a: Int, b: Int): Int = a * b

/*
* Anda juga bisa membuat method dengan parameter sebuah lambda https://kotlinlang.org/docs/lambdas.html#lambda-expressions-and-anonymous-functions
* */
fun perkalianRandom(perkalian: (Int, Int) -> Int): Int {
    return perkalian(2, 10)
}

fun main() {
    /*
    * Dari sebuah kelas anda dapat mendefinisikan berbagai macam object
    * Seperti contoh dibawah ini
    * Anda membuat object car, motor dan bicycle
    * */

    //Cara set sebuah variable dari kelas tersebut
    val car = Vehicle()
    car.name = "Avanza"
    car.model = "SUV"
    car.typeFuel = "Bensin"

    //Anda juga bisa menggunakan set variable melalui constructornya
    val motor = Vehicle(model = "Motor Matic")
    motor.name = "Honda"
    //set sebuah variable melalui enum class
    motor.color = Color2.RED.color
    motor.colorType = Color.RED

    motor.printNamaKendaraan("Honda")

    /*
    * Untuk memanggilnya anda bisa langsung memanggil variabel secara langsung
    * Atau anda bisa panggil methodnya secara langsung.
    * */
    val variable = motor.name
    println(variable)
    //atau anda bisa panggil method secara langsung
    motor.display()

    val bicycle = Vehicle()

    perkalian(5, 10)
    perkalianRandom { i, i2 -> i * i2 }

    /*
    * Anda bisa mendefinisikan lambda secara langsung seperti dibawah
    * perkalian -> nama lambdanya
    * (Int, Int) -> type data untuk parameternya
    * Unit -> type data dari variable lambdanya
    * */
    val perkalian: (Int, Int) -> Unit = { yangDicari, nilaiMax ->
        for (i in 0..nilaiMax) {
            if (i == yangDicari) {
                println("Ketemu")
            }
        }
    }

    val xy: (Int, Int) -> Int = { a, b -> a * b }
    perkalian(5, 10)

}